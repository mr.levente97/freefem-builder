#!/bin/bash

RED='\033[0;31m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

if [[ "$EUID" != 0 ]]; then
    echo -e "${RED} Please run the script as root ${NC}"
    exit 1
fi

stty -echoctl 2>/dev/null # hide ^C

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT
trap ctrl_c SIGINT

function ctrl_c() {
        echo -e "${CYAN} Detected CTRL+C stopping container ${NC}"
        docker stop freefem_builder 2>/dev/null
}


# only build the image if it doesn't exist yet
if [[ "$(docker images -q freefem_builder 2> /dev/null)" == "" ]]; then
    echo -e "${CYAN} freefem_builder image doesn't exist, running docker build ${NC}"
    docker build -t freefem_builder .
else
    echo -e "${CYAN} freefem_builder image found, not running docker build ${NC}"
fi


echo -e "${CYAN} Cleaning up old build files if any ${NC}"

rm -rf "$(pwd)"/freefem/* 2>/dev/null
rm -rf "$(pwd)"/freefem/.* 2>/dev/null

chmod +x "$(pwd)"/entrypoint.sh
mkdir "$(pwd)"/freefem 2>/dev/null
cp "$(pwd)"/entrypoint.sh "$(pwd)"/freefem/entrypoint.sh


if [ "$(docker ps -a -q -f name=freefem_builder)" ]; then
    echo -e "${CYAN} Docker container exists, removing it${NC}"
    docker rm $(docker ps -a -q -f name=freefem_builder) > /dev/null 2>&1
fi

echo -e "${CYAN} Starting container to build FreeFem ${NC}"

docker run -v "$(pwd)/freefem":/freefem --name freefem_builder freefem_builder

if [ $? -ne 0 ]; then
    echo -e "${RED} if you wish to inspect them the log files are found in ./freefem/logs ${NC}"
    exit 1
fi

echo -e "All files are found in ./freefem now you should run:\n \
cd ./freefem & sudo make install"
