# freefem-builder

## TLDR; sudo ./build_and_run.sh && cd ./freefem && sudo make install

## How it works

- Creates a docker image from archlinux/archlinux
- Installs the dependencies for Petsc and Freefem
    <br> *git openmpi gcc-fortran wget python freeglut m4 make patch gmm blas lapack hdf5 gsl fftw arpack suitesparse gnuplot autoconf automake bison flex gdb boost python-numpy valgrind cmake texlive-most unzip m4 bison flex patch*
- Creates or cleans ./freefem
- Starts the container and entrypoint.sh that builds Petsc and FreeFem
