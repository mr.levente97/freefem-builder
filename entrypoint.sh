#!/bin/bash 

RED='\033[0;31m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

stty -echoctl 2>/dev/null # hide ^C

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT
trap ctrl_c SIGINT

function ctrl_c() {
        echo -e "${CYAN} Detected CTRL+C, the script will exit ${NC}"
        exit 1
}

console_log() {
    echo -e "${CYAN} $1 ${NC}"
}

check_error() {
if [ $? -ne 0 ]; then
    echo -e "${RED} Something went wrong ${NC}"
    exit 1
fi
}

LOGS_FOLDER=/freefem/logs

cd /freefem

mkdir /freefem/logs 2>/dev/null

export FF_DIR=${PWD}/FreeFem-sources
export PETSC_DIR=${PWD}/petsc
export PETSC_ARCH=arch-FreeFem
export PETSC_VAR=${PETSC_DIR}/${PETSC_ARCH}
export PETSC_ARCH=arch-FreeFem-complex


console_log "Cloning FreeFem and petsc"

git clone https://github.com/FreeFem/FreeFem-sources.git >>${LOGS_FOLDER}/git.log 2>&1
git clone https://gitlab.com/petsc/petsc.git >>${LOGS_FOLDER}/git.log 2>&1

console_log "Configuring petsc"

cd ${PETSC_DIR}

./configure --download-mumps --download-parmetis --download-metis\
 --download-hypre --download-superlu --download-slepc --download-hpddm --download-ptscotch\
  --download-suitesparse --download-scalapack --download-tetgen --with-fortran-bindings=no\
   --with-scalar-type=real --with-debugging=no >>${LOGS_FOLDER}/petsc_configure.log 2>&1

check_error

console_log "Compiling petsc"

make >>${LOGS_FOLDER}/petsc_compile.log 2>&1

check_error

console_log "Configuring petsc 2nd run"

./configure --with-mumps-dir=arch-FreeFem --with-parmetis-dir=arch-FreeFem\
 --with-metis-dir=arch-FreeFem --with-superlu-dir=arch-FreeFem --download-slepc\
  --download-hpddm --with-ptscotch-dir=arch-FreeFem --with-suitesparse-dir=arch-FreeFem\
   --with-scalapack-dir=arch-FreeFem --with-tetgen-dir=arch-FreeFem --with-fortran-bindings=no\
    --with-scalar-type=complex --with-debugging=no >>${LOGS_FOLDER}/petsc_configure_second.log 2>&1

check_error

console_log "Compiling petsc 2nd run"

make >>${LOGS_FOLDER}/petsc_compile_second.log 2>&1

check_error

cd ${FF_DIR}

# Don't checkout dev, we buildin 4.11 from master
#git checkout develop

console_log "Running FreeFem autoreconf"

autoreconf -i >>${LOGS_FOLDER}/freefem_autoreconf.log 2>&1

check_error

console_log "Running FreeFem configure"

./configure --without-hdf5 --enable-download_metis --enable-download_parmetis\
 --enable-download_mmg --enable-download_mmg3d  --enable-download_parmmg\
  --enable-download_mshmet --enable-download_nlopt --enable-optim\
   --with-petsc=${PETSC_VAR}/lib\
    --with-petsc_complex=${PETSC_VAR}-complex/lib >>${LOGS_FOLDER}/freefem_configure.log 2>&1

check_error

console_log "Running FreeFem 3rdparty getall"
# Added from original guide (https://doc.freefem.org/introduction/installation.html#compilation-on-arch-linux)
./3rdparty/getall -a >>${LOGS_FOLDER}/freefem_3rdparty_getall.log 2>&1

check_error

# Don't make petsc because we already did

#cd 3rdparty/ff-petsc
#make petsc-slepc SUDO=sudo
#cd -

console_log "FreeFem reconfigure"

./reconfigure >>${LOGS_FOLDER}/freefem_reconfigure.log 2>&1

check_error

console_log "Compiling FreeFem"

make >>${LOGS_FOLDER}/freefem_compile.log 2>&1

check_error

#export PATH=${PATH}:${FF_DIR}/src/mpi 
#export PATH=${PATH}:${FF_DIR}/src/nw 
# TODO setup ~/.freefem++.pref 
# https://stackoverflow.com/questions/13046624/how-to-permanently-export-a-variable-in-linux
##### need to add to etc/bash.bashrc (maybe etc/bashrc etc/profile ~/.bashrc ....) UNCOMMENTED
# FF_DIR=~/../home/aszabo/FFinstall/FreeFem-sources
# export PATH="${PATH}:${FF_DIR}/src/mpi" 
# export PATH="${PATH}:${FF_DIR}/src/nw" 
##### create etc/freefem++.pref; and write in it the following:
# loadpath = "/home/andras/FFinstall/FreeFem-sources/plugin/mpi/"
# loadpath += "/home/andras/FFinstall/FreeFem-sources/plugin/seq/"
# includepath = "/home/andras/FFinstall/FreeFem-sources/idp/"
