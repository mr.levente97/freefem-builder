FROM archlinux/archlinux

USER root

RUN sed -ie 's/^\[options\]$/&\nParallelDownloads = 50/g' /etc/pacman.conf && \
echo -e "\033[0;36m Running pacman -Syu \033[0m" && \
pacman -Syu >>/var/log/init_update.log 2>&1 && \
echo -e "\033[0;36m Installing dependencies for FreeFem \033[0m" && \
pacman --noconfirm -S git openmpi gcc-fortran wget python freeglut m4 make patch gmm blas \
lapack hdf5 gsl fftw arpack suitesparse gnuplot autoconf automake bison flex gdb \
boost python-numpy valgrind cmake texlive-most unzip m4 bison flex patch >>/var/log/init_install.log 2>&1


ENTRYPOINT ["/freefem/entrypoint.sh"]
